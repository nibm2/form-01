function validate() {
      
    if( document.myForm.name.value == "" ) {
       alert( "Please provide your Name!" );
       return false;
    }
	if( document.myForm.address.value == "" ) {
       alert( "Please provide your Address!" );
       return false;
	}
    if( document.myForm.contactno.value == "" ) {
       alert( "Please provide your Contact No!" );
       return false;
    }
	if( document.myForm.description.value == "" ) {
       alert( "Please provide your Description!" );
       return false;
    }
    if( document.myForm.country.value == "" ) {
      alert( "Please provide your Country!" );
      return false;
   }
    return( true );
 }